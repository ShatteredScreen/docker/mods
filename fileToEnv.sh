#!/bin/bash

# Set an environment variable value from a file
#
# param $1  The name of the environment variable that will be assigned
setEnvFromFile() {
  local envName="$1"
  local envFileName="${envName}_FILE"

  if [ "${!envName:-}" ] && [ "${!envFileName:-}" ]; then
    echo >&2 "Error: Both $envName and $envFileName are set, please use one or the other."
    exit 1
  fi

  local val=""
  if [ "${!envName:-}" ]; then
    val="${!envName}"
  elif [ "${!envFileName:-}" ]; then
    val="$(< "${!envFileName}")"
  fi

  export "$envName"="$val"
  unset "$envFileName"
}